<?php

/* Child theme declaration */

function iut_wp_enqueue_scripts() {

    $parenthandle = 'twentynineteen-style';
    $theme        = wp_get_theme();

    // Load parent CSS
    wp_enqueue_style(
        $parenthandle,
        get_template_directory_uri() . '/style.css', // https://iut.org/wp-content/themes/twentynineteen/style.css
        array(),
        $theme->parent()->get( 'Version' )
    );

    // Load child CSS (this theme)
    wp_enqueue_style(
        'iut-style',
        get_stylesheet_uri(), // https://iut.org/wp-content/themes/iut/style.css
        array( $parenthandle ),
        $theme->get( 'Version' )
    );

}

add_action( 'wp_enqueue_scripts', 'iut_wp_enqueue_scripts' );

function iut_register_post_type_recette() {

	register_post_type(
		'recette',
		array(
			'labels'				=> array(
				'name'					=> 'Recettes',
				'singular_name'			=> 'Recette',
			),
			'public'				=> true,    // false = cachée de l'interface d'admin et du frontend
			'publicly_queryable'	=> true,    // Visible côté frontend ?
			'show_in_rest'			=> true,	// Nécessaire pour fonctionner avec Gutenberg
			'hierarchical'			=> false,
			'supports'				=> array( 'title', 'editor', 'thumbnail' ),
			'has_archive'			=> 'recettes',
			'rewrite'				=> array( 'slug' => 'recette' ),
		)
	);

}

add_action( 'init', 'iut_register_post_type_recette', 10 );


function iut_add_meta_boxes_recette( $post ) {

	add_meta_box(
		'iut_mbox_recette',                // Unique ID
		'Infos complémentaires',  // Box title
		'iut_mbox_recette_content', 		// Content callback, must be of type callable
		'recette'                          	// Post type
	);

}

add_action( 'add_meta_boxes', 'iut_add_meta_boxes_recette' );

function iut_mbox_recette_content( $post ) {

	// Get meta value
	$value = get_post_meta( $post->ID, '_wporg_meta_key', true );

	echo '<p>';
	echo '<label for="wporg_field">';
	echo 'Ingrediants';
	echo '<textarea id="wporg_field" name="wporg_field" value="">';
	echo '</label>';
	echo '</p>';

}


// Save post meta
function plop_save_post( $post_id ) {

	if ( isset( $_POST['plop-ingrediants'] ) && !empty( $_POST['plop-ingrediants'] ) ) {

		update_post_meta(
			$post_id,
			'plop-ingrediants',
			sanitize_text_field( $_POST['plop-ingrediants'] )
		);


	}

}

add_action( 'save_post', 'plop_save_post' );
